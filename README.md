# weather-app

FIXME: This is a weather API for fething and generating specified reports

## Installation

Download from http://example.com/FIXME.

## Usage

FIXME: explanation

    $ java -jar weatherAPI.jar [args]

## Options
The app accepts 3 different args, only the first one is mandatory i.e report type you want

  First arg

today : To generate todays report from the six listed cities
  
forecast : To generate a 5 day forecast report 
  
Example : java  -jar weatherAPI.jar today

        : java  -jar weatherAPI.jar forecast
        
  Second arg

/home/dennis/documents : You can specify the path where you want your report to be generate 

If not specified the csv file will be generated in the same folder you run the app

Example : java  -jar weatherAPI.jar today /home/gatu

  Third arg

true : When you want generated csvs to be loaded to your postgres DB 

For the csv to be loaded ok you will need to add your config file path when starting the app and have created 
the two different tables before execution. CHECK weather-api-tables.sql file for app SCHEMA

Example : java -Dconfig="/home/gatu/weather-app.config" -jar weatherAPI.jar today /home/gatu true

Note, Inclusion of a config file does not translate to csv report generated being loaded to the DB if the third parameter is not set to true
 

## Examples
The files generated have a timespamp extension for easier readability:

todays_weather[2021-01-29-10:31].csv

forecast_weather[2021-01-29-10:32].csv
...

### Bugs
Feel free to reach out if any is encountered. Happy weather reporting :)
...

### Any Other Sections
### That You Think
### Might be Useful

## License

Copyright © 2021 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
