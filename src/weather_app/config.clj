(ns weather-app.config
  (:require                                                 ;[clojure.tools.logging :as log]
            [environ.core :refer [env]])
  (:import [java.util Properties]
           [java.io FileInputStream])
  (:use [clojure.walk]))
;
(defn read-properties [key]
  (let [file-input-stream (new FileInputStream (env key))
        properties (Properties.)]
    (-> properties (.load file-input-stream))
    (keywordize-keys (into {} properties))))

(def config-file-data (atom {}))

(def get-value (fn [ky dflt] (get @config-file-data ky dflt)))

(defn load-config-file []
  "Initializing config file"
  (reset! config-file-data (read-properties :config)))

;; Database start-up Configs
(def db-name (get-value :db-name "acs_test"))
(def db-port (get-value :db-port 5432))
(def db-ip (get-value :db-ip "127.0.0.1"))
(def db-user (get-value :db-user "postgres"))
(def db-password (get-value :db-password "postgres"))
(def db-url (str "postgresql://" db-user ":" db-password "@" db-ip ":" db-port "/" db-name "?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory"))

;;Weather App creds
(def open-weather-url (get-value :open-weather-url  "http://api.openweathermap.org/data/2.5/"))
(def open-weather-key (get-value :open-weather-key  "cd03cb60b51aab3f021d5ebb95ebf47e"))
(def open-weather-conn-timeout (get-value :open-weather-conn-timeout  3000))

;;;;Test city cases IDs(this are the unique ids for Athens,Paris,London,Madrid,Moscow and Rome respectively)
(def test-cities (get-value :test-cites  "264371,2988507,2643743,3117735,524901,3169070"))