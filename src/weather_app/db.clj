(ns weather-app.db
  (:require                                                 ;[clojure.tools.logging :as log]
    [weather-app.config :refer :all]
    [clojure.java.jdbc :as sql]
    [clojure.string :as str]
    [clojure.java.io :as io])
  (:import org.postgresql.copy.CopyManager))


(def db-initialized? (atom false))
(def copy-manager (atom nil))


;; Initialize function
(defn init-db []
  "Creating conn to the DB"
  (try
    (reset! copy-manager (CopyManager. (sql/get-connection db-url)))
    (reset! db-initialized? true)
    (catch Exception er
      (println "Database Error ---" (.getMessage er)))))



(defn copy-csv [file db-name]
  (try
    (with-open [file-reader (io/reader file)]
      (let [sqll (str "copy " db-name " from STDIN with CSV HEADER")
            _ (if (nil? @copy-manager) (init-db))]
        (if (.copyIn @copy-manager sqll file-reader)
          (do (println (format "File [%s] loaded ok on [%s]" file db-name)) 0)
          (do (println (format "Error loading [%s] to the DB" file) 1)))))
    (catch Exception e
      (do (println (format "!copy-[%s] into [%s]" file db-name) e) -1))))


(defn load-csv [fl-name]
  (cond
    (false? @db-initialized?) (println "Unable to load CSV due to missing DB uplink")
    (str/includes? fl-name "todays_weather") (copy-csv fl-name "tbl_today_weather_report")
    (str/includes? fl-name "forecast_weather") (copy-csv fl-name "tbl_forecast_weather_report")
    :otherwise (do  (println "Filename not allowed for DB loading : " fl-name) -1)
    ))