(ns weather-app.handler
  (:require [org.httpkit.client :as http]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [clj-time.format :as fmt]
            [clj-time.core :as tm-cr]
            [clj-time.coerce :as c]
            [clojure.core.reducers :as r]
            [weather-app.config :refer :all]
            [weather-app.db :as db]
            [com.rpl.specter :refer :all]))

(declare now-weather-processor forecast-weather-processor prepare-forecast-csv)

(defn connWeatherAPI
  [call-type city]
  (try
    (let [{:keys [status body error]} @(http/post (str open-weather-url call-type)
                                                  {:query-params {:appid open-weather-key :id city} :timeout open-weather-conn-timeout})
          body (if-not error (json/read-str body :key-fn keyword) error)]
      (if error [true :internet-error]
      (if (= status 200)
        [false body]
        (do
          (println (format "Error retrieving weather info. Status [%s] : ErrorMessage [%s]"
                           (:cod body) (:message body)))
          [true :connection-error]))))
    (catch Exception ex
      (println "Error connecting to WeatherAPI : " (.getMessage ex))
      [true :processing-error])))

(defn results ([] []) ([xs x] (into xs x)))

(defn getWeatherInfo
  [user-args]
  (let [[report-type output-dir] user-args
        ;;Confirm output path exists else default to .
        output-dir (if (and output-dir (.isDirectory (io/file output-dir)))
                     output-dir (do
                                  (println (format "WARNNING! The CSV output path [%s] does not exist" output-dir))
                                  (System/getProperty "user.dir")))]
    (condp = (keyword report-type)
      :today  (-> (connWeatherAPI "group" test-cities) (now-weather-processor output-dir))
      :forecast  (-> (r/fold results
                             (pmap #(forecast-weather-processor (connWeatherAPI "forecast" %)) (str/split test-cities #",")))
                     (prepare-forecast-csv output-dir))
      (throw (RuntimeException. (str "Unknown user request => " (first user-args)))))))

;;---------------
(declare kelvin-to-degrees unix-to-date generate-csv date-extension)

(defn now-weather-processor
  [[error? results] outp-dir]
  (if error? (do (println "Error! processing req => " results) -1)
    (let [
          ;;transform temps in results
          data (transform [:list ALL :main (submap [:temp :feels_like :temp_min :temp_max]) MAP-VALS]
                     #(kelvin-to-degrees %) results)
          processor (fn [data]
                      (let [{:keys [coord sys weather name dt wind
                                    id weather clouds main visibility timezone]} data
                            {city_longitude :lon city_latitude :lat} coord
                            {wind_speed :speed wind_direction :deg wind_gust :gust} wind
                            {weather_cond_id :id weather_group :main weather_description :description
                             weather_icon :icon} (first weather)
                            {cloudiness_percentage :all} clouds
                            {city_country_code :country sunrise_time :sunrise city_sunset :sunset} sys
                            {temp :temp  human_temp_feels_like :feels_like atmospheric_pressure :pressure
                             min_temp :temp_min  max_temp :temp_max  humidity :humidity sea_level_atms_pressure :sea_level
                             ground_level_atms_pressure :grnd_level} main
                            [dt sunrise_time city_sunset] (map #(unix-to-date %) [dt sunrise_time city_sunset])
                            [current_temp human_feels_like_temp min_temp max_temp] (map #(kelvin-to-degrees %)
                                                                                        [temp human_temp_feels_like min_temp max_temp])]
                        {:city_name name :city_id id :city_country_code city_country_code :weather_timestamp dt :city_longitude city_longitude :city_latitude city_latitude
                         :wind_speed wind_speed :wind_direction wind_direction :wind_gust wind_gust :cloudiness_percentage cloudiness_percentage :weather_group weather_group
                         :weather_cond_id weather_cond_id :weather_description weather_description :weather_icon weather_icon :atmospheric_pressure atmospheric_pressure
                         :humidity humidity :sea_level_atms_pressure sea_level_atms_pressure :ground_level_atms_pressure ground_level_atms_pressure :sunrise_time sunrise_time
                         :sunset_time city_sunset :city_temp current_temp :human_feels_temp human_feels_like_temp :min_temp min_temp :max_temp max_temp :visibility visibility
                         }))
          final-data (future (reduce #(conj %1 (processor %2))
                                     [] (-> results :list)))
          columns (-> @final-data first keys vec)
          file-nm (str outp-dir "/todays_weather" date-extension ".csv")
          ]
       (generate-csv file-nm columns @final-data)
       (println "todays_weather_report generated ok =>  " file-nm)
      )
    )
  )

(defn forecast-weather-processor
  [[error? results]]
  (let [final-result (if error? (do
                                  (println "Unable to load city forecast :" results)
                                  ;skip processing the failed request
                                  -1)
                                (let [{:keys [city list cnt]} results
                                      {city_id :id city_name :name city_country_code :country sunset :sunset coord :coord
                                       city_population :population city_timezone :timezone sunrise :sunrise} city
                                      {city_longitude :lon city_latitude :lat} coord
                                      [sunset_time sunrise_time] (map #(unix-to-date %) [sunset sunrise city_timezone])
                                      args {:city_id city_id :city_name city_name :city_country_code city_country_code :city_population city_population
                                            :city_timezone city_timezone :sunrise_time sunrise_time :sunset_time sunset_time
                                            :timestamp_count cnt :city_longitude city_longitude :city_latitude city_latitude}
                                      list-results (reduce (fn [rslt data]
                                                             (conj rslt
                                                                   (let [{:keys [dt_txt pop wind weather clouds sys main visibilty rain snow main]} data
                                                                         {wind_speed :speed wind_direction :deg wind_gust :gust} wind
                                                                         dt_txt (c/from-string dt_txt)
                                                                         {weather_cond_id :id weather_group :main weather_description :description
                                                                          weather_icon :icon} (first weather)
                                                                         {cloudiness_percentage :all} clouds
                                                                         {part_of_day :pod} sys
                                                                         {temp :temp  human_temp_feels_like :feels_like atmospheric_pressure :pressure
                                                                          min_temp :temp_min  max_temp :temp_max  humidity :humidity sea_level_atms_pressure :sea_level
                                                                          ground_level_atms_pressure :grnd_level} main
                                                                         {rain_vol_in3H :3h} rain
                                                                         {snow_vol_in3H :3h} snow]
                                                                     (merge args
                                                                            {:wind_speed wind_speed :wind_direction wind_direction  :wind_gust wind_gust :weather_cond_id weather_cond_id
                                                                             :weather_group weather_group :weather_description weather_description :weather_icon weather_icon :max_temp max_temp
                                                                             :cloudiness_percentage cloudiness_percentage  :city_temp temp :human_feels_temp human_temp_feels_like :min_temp min_temp
                                                                             :atmospheric_pressure atmospheric_pressure :humidity humidity :sea_level_atms_pressure sea_level_atms_pressure :day_or_night part_of_day
                                                                             :ground_level_atms_pressure ground_level_atms_pressure :rain_vol_in3H rain_vol_in3H :snow_vol_in3H snow_vol_in3H
                                                                             :visibility visibilty :recording_date dt_txt}))))
                                                           [] list)]

                                  list-results
                                  ))]
    final-result
    ))

(defn prepare-forecast-csv [data outp-d]
  (let [file-nm (str outp-d "/forecast_weather" date-extension ".csv")
        columns (-> data first keys vec)]
    (generate-csv file-nm columns data)
    (println "forecast_weather_report generated ok => " file-nm)))

;;-----------------
(defn kelvin-to-degrees
  ;0 Kelvin = -273.15 °C
  [temp]
  (if (nil? temp) nil (- temp 273.15)))

(defn unix-to-date
  [unix-time]
  (if (nil? unix-time) nil (c/from-long (* unix-time 1000))))

(defn string-to-date [stg]
  (fmt/parse (fmt/formatter "YYYY-MM-dd") stg))

(def date-extension
  (.format
    (java.text.SimpleDateFormat. "[yyyy-MM-d-hh:mm]") (new java.util.Date)))


(defn generate-csv [file columns data]
  (let [headers (map name columns)
        rows (mapv #(mapv % columns) data)]
    (with-open [file (io/writer file)]
      (csv/write-csv file (cons headers rows))))
    (when @db/db-initialized? (db/load-csv file))
  nil
  )


(def data
  [false
   {:cnt 6,
    :list [{:coord {:lon 23.7162, :lat 37.9795},
            :name "Athens",
            :dt 1612494546,
            :wind {:speed 1.08, :deg 31},
            :id 264371,
            :weather [{:id 800, :main "Clear", :description "clear sky", :icon "01n"}],
            :clouds {:all 0},
            :sys {:country "GR", :timezone 7200, :sunrise 1612502759, :sunset 1612540339},
            :main {:temp 284.34, :feels_like 283.39, :temp_min 282.15, :temp_max 286.48, :pressure 1018, :humidity 87},
            :visibility 10000}
           {:coord {:lon 2.3488, :lat 48.8534},
            :name "Paris",
            :dt 1612494572,
            :wind {:speed 1.54, :deg 190},
            :id 2988507,
            :weather [{:id 701, :main "Mist", :description "mist", :icon "50n"}],
            :clouds {:all 90},
            :sys {:country "FR", :timezone 3600, :sunrise 1612509300, :sunset 1612544055},
            :main {:temp 280.66, :feels_like 279, :temp_min 280.15, :temp_max 281.15, :pressure 1010, :humidity 100},
            :visibility 3100}
           {:coord {:lon -0.1257, :lat 51.5085},
            :name "London",
            :dt 1612494575,
            :wind {:speed 1.54, :deg 0},
            :id 2643743,
            :weather [{:id 804, :main "Clouds", :description "overcast clouds", :icon "04n"}],
            :clouds {:all 90},
            :sys {:country "GB", :timezone 0, :sunrise 1612510340, :sunset 1612544203},
            :main {:temp 280.79, :feels_like 278.92, :temp_min 280.37, :temp_max 281.15, :pressure 1007, :humidity 93},
            :visibility 10000}
           {:coord {:lon -3.7026, :lat 40.4165},
            :name "Madrid",
            :dt 1612494569,
            :wind {:speed 1.54, :deg 60},
            :id 3117735,
            :weather [{:id 800, :main "Clear", :description "clear sky", :icon "01n"}],
            :clouds {:all 0},
            :sys {:country "ES", :timezone 3600, :sunrise 1612509609, :sunset 1612546651},
            :main {:temp 282.4, :feels_like 280.44, :temp_min 280.15, :temp_max 283.71, :pressure 1008, :humidity 81},
            :visibility 10000}
           {:coord {:lon 37.6156, :lat 55.7522},
            :name "Moscow",
            :dt 1612494426,
            :wind {:speed 5, :deg 330},
            :id 524901,
            :weather [{:id 600, :main "Snow", :description "light snow", :icon "13n"}],
            :clouds {:all 75},
            :sys {:country "RU", :timezone 10800, :sunrise 1612502142, :sunset 1612534283},
            :main {:temp 264.15, :feels_like 257.46, :temp_min 263.15, :temp_max 265.15, :pressure 1002, :humidity 79},
            :visibility 10000}
           {:coord {:lon 12.4839, :lat 41.8947},
            :name "Rome",
            :dt 1612494583,
            :wind {:speed 1.54, :deg 60},
            :id 3169070,
            :weather [{:id 800, :main "Clear", :description "clear sky", :icon "01n"}],
            :clouds {:all 0},
            :sys {:country "IT", :timezone 3600, :sunrise 1612505904, :sunset 1612542586},
            :main {:temp 282.49, :feels_like 281.01, :temp_min 281.15, :temp_max 283.15, :pressure 1017, :humidity 93},
            :visibility 10000}]}])
