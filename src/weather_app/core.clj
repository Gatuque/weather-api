(ns weather-app.core
  ^{:author "George Gatuma"
    :created  "26th January 2021"
    :title "Weather API (KleeneAI Interview"}
  (:require [weather-app [handler :as hdl]
                         [db :as db]])
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (if (empty? args)
    (throw (NullPointerException. "What forecast do you want. \"today\" or \"forecast\" and path to your csv results"))
    (do
      ;;Use the third option as an external config file flag, if nil proceed to process with app defaults with loading output csv the DB
      (let [[report_type path load-conf?] args]
        (when (= load-conf? "true") (db/init-db))
        (hdl/getWeatherInfo [report_type path])))
    )
  )
