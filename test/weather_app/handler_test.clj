(ns weather-app.handler-test
  (:require [clojure.test :refer :all]
            [midje.sweet :refer :all]
            [weather-app.handler :refer :all]
            [weather-app.handler-data-samples :refer :all]
            [clj-http.client :as http]
            [clojure.core.reducers :as r]
            [clojure.java.io :as io]
            [weather-app.config :refer :all]
            [weather-app.db :as db]))

(deftest connWeatherAPI_test
  (with-redefs [println (constantly nil)]
    (testing "A the wrong URI"
      (is (= (first (connWeatherAPI "tody" "12345")) true))
      (is (= (first (connWeatherAPI "TODAY" "12345")) true))
      (is (= (first (connWeatherAPI "Forecast" "12345")) true)))

    (testing "wrong user-id"
      (with-redefs [open-weather-key "1234567"]
        (is (= (first (connWeatherAPI "group" 524901)) true))
        (is (= (second (connWeatherAPI "group" 524901)) :connection-error))
        ))


    (testing "Testing a city id that does not exist"
      (is (= (first (connWeatherAPI "group" "12345")) true))
      (is (= (first (connWeatherAPI "group" "London,Moscow")) true))
      (is (= (second (connWeatherAPI "group" "12345")) :connection-error))
      (is (= (first (connWeatherAPI "forecast" "4567")) true))
      (is (= (first (connWeatherAPI "forecast" "99,hd")) true))
      (is (= (-> (connWeatherAPI "forecast" "99,hd") last type) clojure.lang.Keyword)))

    (testing "Passing correct args testing"
      (is (= (first (connWeatherAPI "group" "524901,2988507")) false))
      (is (= (-> (connWeatherAPI "group" "264371,2988507,2643743") second type) clojure.lang.PersistentArrayMap))
      (is (= (first (connWeatherAPI "forecast" "3117735")) false))
      (is (= (first (connWeatherAPI "group" "3117735")) false))
      )))

(deftest results_test
  (testing "Always returns an empty vector on first call of reduction"
    (is (= (results) []))
    (is (= (type (results)) clojure.lang.PersistentVector)))

  (testing "Test it builds a final vec ok"
    (is (= (results [] [1]) [1]))
    (is (= (results [1] [2]) [1 2]))
    (is (thrown? IllegalArgumentException (results [1 2] 4)))
    (is (= (results [{:city "London" :Rain 45 :Wind 67}] [{:city "Athens" :Rain 78 :Wind 89}])
           [{:city "London" :Rain 45, :Wind 67} {:city "Athens" :Rain 78, :Wind 89}])))
  (testing "Arity Check"
    (is (thrown? Exception (results [])))
    (is (thrown? Exception (results {:city :A :rain 90})))
    ))

(def callAPI (fn [inpt] (getWeatherInfo inpt)))
(def check-file? (fn [file] (.exists (io/as-file file))))

(deftest getWeatherInfo_test
  (with-redefs [db/load-csv (fn [fl-nm] nil)]
    (testing "generate-csv function"
      (let [data [{:a 80 :b 2910 :c 45870} {:a 1200 :b 500 :c 7865}]
            columns (-> data first keys vec)]
        (= (type data) clojure.lang.PersistentVector)
        (= (every? keyword? columns) true)

        (testing "CSV files generation"
          (is (and (nil? (generate-csv "Test-file2.csv" columns data))
                   (check-file? (str (System/getProperty "user.dir") "/Test-file2.csv"))) true)
          (is (and (nil? (generate-csv "/tmp/Test-file.csv" columns data))
                   (check-file? "/tmp/Test-file.csv" )) true)
          (is (thrown? java.io.FileNotFoundException (generate-csv "/temp/test.csv" columns data)))
          ))))

  (with-redefs [println (constantly nil)]
    (testing "Wrong weather type request"
      (is (thrown? RuntimeException (getWeatherInfo ["todai"])))
      (is (= (try (.getMessage (getWeatherInfo ["focast"])) (catch Exception ex (.getMessage ex)))
             "Unknown user request => focast"))
      (is (= (try (.getMessage (getWeatherInfo ["tomorrow"])) (catch Exception ex (.getMessage ex)))
             "Unknown user request => tomorrow")))

    (testing "testing 'today' weather requests"
      (with-redefs [connWeatherAPI (fn [wt-type city] today-getWeather-data)
                    now-weather-processor (fn [dt dir] (generate-csv (str dir today-file-name)
                                                                     today-sample-columns today-nowWeather-data))]
        (= (type today-getWeather-data) clojure.lang.PersistentVector)
        (= (every? keyword? today-sample-columns) true)

        (testing "correct csv path"
          (is (and (= (callAPI ["today" "/tmp"]) nil)
                   (check-file? (str "/tmp" today-file-name))) true)
          ;(is (and (nil? (callAPI ["today" "test-dir/gatu"]))
          ;         (check-file? (str "test-dir/gatu" today-file-name))) true)
          )

        (testing "One arg user request. Unspecified csv path"
          (is (and (nil? (callAPI ["today"]))
                   (= (check-file? (str (System/getProperty "user.dir") today-file-name)) true)) true))

        (testing "Testing a non-existant csv path dir"
          (is (and (nil? (callAPI ["today" "test-dir/GEORGE"]))
                   (check-file? (str (System/getProperty "user.dir") today-file-name))) true))))


    (testing "testing 'forecast' weather requests"
      (with-redefs [connWeatherAPI (fn [wt-type city] focast-getWeather-data)
                    r/fold (fn [rslt coll] forecast-weather-processor-data)
                    prepare-forecast-csv (fn [dt dir] (generate-csv (str dir forecast-file-name)
                                                                    forecast-sample-columns forecast-weather-processor-data))]
        (= (type focast-getWeather-data) clojure.lang.PersistentVector)
        (= (every? keyword? forecast-sample-columns) true)

        (testing "Correct csv path"
          ;(is (and (= (callAPI ["forecast" "test-dir/gatu"]) nil)
          ;         (check-file? (str "test-dir/gatu" forecast-file-name))) true)
          (is (and (= (callAPI ["forecast" "/tmp"]) nil)
                   (check-file? (str "/tmp" forecast-file-name))) true))

        (testing "One arg user request. Unspecified csv path"
          (is (and (= (callAPI ["forecast"]) nil)
                   (check-file? (str (System/getProperty "user.dir") forecast-file-name))) true))

        (testing "Testing a non-existant csv path dir"
          (is (and (= (callAPI ["forecast" "/hmz/GATUMA"]) nil)
                   (check-file? (str (System/getProperty "user.dir") forecast-file-name))) true))))))

(deftest now-weather-processor_test
  (with-redefs [println (constantly nil)]
    (testing "Testing error on connWeatherAPI"
      (is (= (now-weather-processor [true :connection-error] "/") -1)))

    (testing "Testing now-weather-processor processing and csv generation"
      (with-redefs [date-extension ""]
        (is (thrown? UnsupportedOperationException (now-weather-processor 34 "/tmp")))
        (is (thrown? UnsupportedOperationException (now-weather-processor {} "/tmp")))
        (is (and (nil? (now-weather-processor today-getWeather-data "/tmp"))
                 (check-file? "/tmp/todays_weather.csv")) true)
        ;(is (and (nil? (now-weather-processor today-getWeather-data "test-dir/gatu"))
        ;         (check-file? (str "test-dir/gatu/todays_weather.csv"))) true)
        (is (thrown? java.io.FileNotFoundException (now-weather-processor today-getWeather-data "test-dir/gAtU")))
        )))

  )

(deftest forecast-weather-processor_test
  (with-redefs [println (constantly nil)]
    (testing "Testing error on connWeatherAPI"
      (is (= (forecast-weather-processor [true :connection-error]) -1)))

    (testing "Testing forecast-weather-processor processing"
      (is (= (type (forecast-weather-processor focast-getWeather-data)) clojure.lang.PersistentVector))
      (is (= (forecast-weather-processor [[false []]]) -1))
      (is (thrown? UnsupportedOperationException (forecast-weather-processor 34)))
      (is (thrown? UnsupportedOperationException (forecast-weather-processor {})))
      (is (= (forecast-weather-processor []) []))
      )
    ))

(deftest prepare-forecast-csv_test
  (with-redefs [date-extension ""
                println (constantly nil)]
    (testing "prepare-forecast-csv processing tests"
      (let [data [{:city "Winterfell" :population 12459}
                  {:city "Kings-landing" :population 3295397}
                  {:city "Casterly Rock" :population 22235}]]
        ;(is (and (nil? (prepare-forecast-csv data "/tmp"))
        ;         (check-file? "/tmp/forecast_weather.csv")) true)
        ;(is (and (nil? (prepare-forecast-csv data "test-dir/gatu/Music"))
        ;         (check-file? (str "test-dir/gatu/Music/forecast_weather.csv"))) true)
        (is (thrown? java.io.FileNotFoundException (prepare-forecast-csv data "test-dir/gAtU")))
        ))))

(deftest kelvin-to-degrees_test
  (testing "kelvin to degress tests"
    (are [kel deg] (= (kelvin-to-degrees kel) deg)
                   0 -273.15
                   424 150.85000000000002
                   300 26.850000000000023
                   23 -250.14999999999998
                   234 -39.14999999999998
                   nil nil
                   )))

(deftest unix-to-date_test
  (testing "unix to date tests"
    (is (= (type (unix-to-date 1611815186)) org.joda.time.DateTime))
    (is (nil? (unix-to-date nil)))))




