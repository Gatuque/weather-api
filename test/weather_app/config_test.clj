(ns weather-app.config-test
  (:require [clojure.test :refer :all]
            [weather-app.config :refer :all]))


(deftest read-properties_test
  (testing "keyword that was not include on project path"
    (is (thrown? NullPointerException (read-properties :conf)))
    (is (thrown? NullPointerException (read-properties "config")))
    (is (thrown? NullPointerException (read-properties :CONFIG))))

  ;Passes since :config is included on path on project.clj
  (is (= (type (read-properties :config)) clojure.lang.PersistentArrayMap))
  )

(deftest load-config-file_test
  (testing "Testing load-config-file"
    (is (= (load-config-file) @config-file-data))
    (is (= (type (load-config-file)) clojure.lang.PersistentArrayMap))))

(deftest getting-conf-value
  (with-redefs [println (constantly nil)]
    (testing "Testing reading values that are in the config"
      (= (get-value :db-port 99) 5432)
      (= (get-value :db-ip "10.23.45.67") "127.0.0.1")
      (= (get-value :db-name "weather_test_DB") "acs_test"))

    (testing "default/hardcoded values"
      (= (get-value :report-table "tbl_weather_report") "tbl_weather_report")
      (= (get-value :rmq-server "127.0.0.1") "127.0.0.1")
      (= (get-value :rmq-xchg "daily_sms") "daily_sms"))
    ))