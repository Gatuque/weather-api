(ns weather-app.core-test
  (:require [clojure.test :refer :all]
            [weather-app.core :refer :all]))

(deftest main-fn-test
  (with-redefs [println (constantly nil)]
    (testing "starting the service with 0 input"
      (is (thrown? NullPointerException (-main))))

    (testing "starting the service with 1 wrong arg, neither today nor forecast"
      (is (thrown? RuntimeException (-main "todai")))
      (is (thrown? RuntimeException (-main "next_month")))
      (is (thrown? RuntimeException (-main "tomorrow"))))

    (testing "starting the service with 1 correct arg"
      (is (= (-main "today") nil)))

    (testing "starting the service with wrong path arg"
      (is (= (-main "today" "/hom") nil)))
    ))
