(ns weather-app.db-test
  (:require [clojure.test :refer :all]
            [weather-app.db :refer :all]
            [weather-app.handler :refer :all]))

(deftest load-csv_test
  (with-redefs [println (constantly nil)]
    (testing "load-csv test samples"
      (= (load-csv "test.csv") -1)
      (= (load-csv "/TESt.csv") -1))))

(def check-file? (fn [file] (.exists (clojure.java.io/as-file file))))

(deftest copy-csv_test
  (with-redefs [println (constantly nil)]
    (testing "Testing of a non existant file or table or both"
      (= (copy-csv "test.csv" "tbl_test_report") -1)
      (= (copy-csv "testerday_forecat_report.csv" "tbl_forecast_weather_report") -1)
      (= (copy-csv "todays_weather[2021-01-29-02:32].csv" "tbl_weather_report") -1))

    (testing "A proper pre-exisiting table on a csv file with the right info
              [create table test_csv (value_A int,value_B int,value_C int)]"
      (let [data [{:a 80 :b 2910 :c 45870} {:a 1200 :b 500 :c 7865}]
            columns (-> data first keys vec)]
        (testing "CSV files generation & DB insert"
          ;(is (and (nil? (generate-csv "Test-file.csv" columns data))
          ;         (check-file? (str (System/getProperty "user.dir") "/Test-file.csv"))
          ;         (= (copy-csv (str (System/getProperty "user.dir") "/Test-file.csv") "test_csv") 0)) true)
          )))))