(defproject weather-app "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [http-kit "2.5.1"]
                 [org.clojure/data.json "1.0.0"]
                 [org.clojure/data.csv "1.0.0"]
                 [clj-time "0.15.2"]
                 [clj-http "3.11.0"]
                 [org.clojure/java.jdbc "0.7.5"]
                 [org.postgresql/postgresql "9.4-1200-jdbc41"]
                 [com.rpl/specter "1.1.3"]
                 [environ "1.2.0"]]
  :main ^:skip-aot weather-app.core
  :target-path "target/%s"
  :uberjar-name "weatherAPI.jar"
  :profiles {:uberjar {:aot :all}
             :dev {:dependencies [[midje "1.9.9"]]
                   :plugins [[quickie "0.4.2"]
                             [lein-midje "3.2.2"]]
                   :jvm-opts ["-Dconfig=weather-app.config"]}})
