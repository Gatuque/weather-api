FROM ubuntu:latest
LABEL [maintainer="georgegatuma.gg@gmail.com" version=1.0]
RUN \
# Update
apt-get update -y && \
# Install Java
apt-get install default-jre -y
ADD  ./target/uberjar/weatherAPI.jar myAPI.jar
ENV arg1 today
ENV arg2 /tmp
RUN echo "Generating ${arg1} weather report"
CMD java -jar myAPI.jar ${arg1} ${arg2}

##docker build -t weather-img:latest -f build.Dockerfile .
##docker run -e arg1="forecast" -e arg2="/tmp" --name weatherAPI weather-img:latest
